﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TeachersNMarks
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'marksDataSet.Mark' table. You can move, or remove it, as needed.
            this.markTableAdapter.Fill(this.marksDataSet.Mark);
            // TODO: This line of code loads data into the 'marksDataSet.Teacher' table. You can move, or remove it, as needed.
            this.teacherTableAdapter.Fill(this.marksDataSet.Teacher);
            // TODO: This line of code loads data into the 'marksDataSet.TeacherSubj' table. You can move, or remove it, as needed.
            this.teacherSubjTableAdapter.Fill(this.marksDataSet.TeacherSubj);
            // TODO: This line of code loads data into the 'marksDataSet.Subj' table. You can move, or remove it, as needed.
            this.subjTableAdapter.Fill(this.marksDataSet.Subj);
            // TODO: This line of code loads data into the 'marksDataSet.Student' table. You can move, or remove it, as needed.
            this.studentTableAdapter.Fill(this.marksDataSet.Student);

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            switch (tabControl.SelectedIndex)
            {
                case 0:
                    markTableAdapter.Update(marksDataSet.Mark);
                    break;
                case 1:
                    studentTableAdapter.Update(marksDataSet.Student);
                    break;
                case 2:
                    teacherTableAdapter.Update(marksDataSet.Teacher);
                    break;
                case 3:
                    subjTableAdapter.Update(marksDataSet.Subj);
                    break;
                case 4:
                    teacherSubjTableAdapter.Update(marksDataSet.TeacherSubj);
                    break;
            }
        }
    }
}
